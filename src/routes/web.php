<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', DashboardController::class)->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});


Route::get('/tasks', [TaskController::class, 'index'])->name('tasks.index');
Route::get('/tasks/add', [TaskController::class, 'create'])->name('tasks.create');
Route::post('/tasks/add/', [TaskController::class, 'store'])->name('tasks.store');
Route::get('/tasks/${id}', [TaskController::class, 'show'])->name('tasks.show');
Route::get('/tasks/${id}/edit', [TaskController::class, 'edit'])->name('tasks.edit');
Route::patch('/tasks/${id}', [TaskController::class, 'update'])->name('tasks.update');
Route::delete('/tasks/${id}/delete', [TaskController::class, 'destroy'])->name('tasks.destroy');

require __DIR__.'/auth.php';
